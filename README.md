# Trailing

On every new line of input to this program, pipe the last 10 lines of input to a new instance of the given program, and echo its output.

## What it does

1. Listens for a line to come in on stdin
1. records in a rolling slice of 10 lines
1. starts the given program
1. Writes the lines to the program's std in
1. Repeat

## How to use it

`<command> | trailing <command that consumes stdin>`

## Example

* graph the average ping time

`ping 1.1.1.1 | trailing datamash mean 1 | asciigraph`

## Disclaimer

This utility was written to scratch an itch, and for the moment, it does.  I'm
not sure when or if I'll have the occassion or need to improve it, but I'd love
to hear if you find it useful and would like more features.
