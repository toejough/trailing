//+build mage

package main

import (
	"github.com/magefile/mage/sh"
)

var Default = Lint

// Lint the codebase
func Lint() error {
	return sh.Run("golangci-lint", "run")
}

// Build the binary
func Build() error {
	return sh.Run("go", "build", "trailing.go")
}
