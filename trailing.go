package main

import (
	"bufio"
	"io"
	"os"
	"os/exec"
	"syscall"

	"github.com/fatih/color"
)

func main() {
	alert := color.New(color.FgRed).FprintfFunc()
	args := os.Args[1:]
	reader := bufio.NewReader(os.Stdin)
	var lines []string
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			alert(os.Stderr, "unable to continue: %v\n", err)
			os.Exit(1)
		}

		lines = append(lines, line)
		length := len(lines)
		if length > 10 {
			lines = lines[length-10:]
		}

		command := exec.Command(args[0], args[1:]...)
		command.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
		command.Stdout = os.Stdout
		command.Stderr = os.Stderr
		stdin, err := command.StdinPipe()
		if err != nil {
			alert(os.Stderr, "unable to continue: %v\n", err)
			os.Exit(1)
		}

		command.Start()

		for _, l := range lines {
			io.WriteString(stdin, l)
		}
		stdin.Close()

		command.Wait()

		exitCode := command.ProcessState.ExitCode()
		if exitCode != 0 {
			os.Exit(exitCode)
		}
	}
}
