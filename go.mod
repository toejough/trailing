module gitlab/toejough/terror

go 1.12

require (
	github.com/fatih/color v1.9.0
	github.com/magefile/mage v1.8.0
)
